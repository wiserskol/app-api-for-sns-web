#KOL App API Specification 

##List of API

###KOL - search_kol_list       搜索kol
###KOL - get_kol_detail        获取kol详情
###MESSAGE - send_message      发送广播/消息
###MESSAGE - get_inbox_list    获取和某个GID有的聊天记录的KOL列表
###MESSAGE - get_event_list    获取某个GID的event列表
###MESSAGE - get_event_detail  获取某个event详情
###MESSAGE - record_read_time  记录kol最新查看消息时间
###MESSAGE - get_message_list  获取消息列表