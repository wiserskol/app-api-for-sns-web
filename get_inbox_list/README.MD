#KOL APP API -get_inbox_list
##接口描述：获取和某个GID有的聊天记录的KOL列表，按照由近到远排序，每次20条。

```
Request Parameters -
snsUser :  sns web 登录帐号
senderUUID : A unique id to identify who is sending this message or event id，Should be md5(snsUser+sendAs)
pageNum : 页码
pageSize : 每页记录数
举例
{
"snsUser":"BJRND3@admin",
"senderUUID":"68565fbee77756886176d373ff679c7d",
"pageNum":1，
"pageSize":20
}
```

----------------------------------------------------------------------

```
Reponse Parameters -
returnCode : Success:50000,Fail:40000,Error:以"E"开头的编码
errorMsg :  如果returnCode返回为非Success,则需要填写相应的错误提示信息
senderUUID : A unique id to identify who is sending this message or event id，Should be md5(snsUser+sendAs)
totalUnRead : 返回snsUser对应的所有未读消息总数，与翻页无关 
kols : 聊天记录的KOL列表
appKolId : app kol id,
appKolName : app kol name,
profileImageUrl : app kol 头像地址
unReadMsgCount ：appp kol给sns web客户发送并且未读的消息总数

举例
{
  "returnCode":"50000",
  "errorMsg":null,
  "senderUUID":"68565fbee77756886176d373ff679c7d",
  "totalUnRead":20,
  "kols":[
    {
      "appKolId": "11",
      "appKolName": "张佳明",
      "profileImageUrl": "http://wwerwer.com/kkk.pgn",
      "unReadMsgCount": 10
    },
    {
      "appKolId": "22",
      "appKolName": "张佳明2",
      "profileImageUrl": "http://wwerwer.com/kkk.pgn",
      "unReadMsgCount": 30
    }
  ]
}
```