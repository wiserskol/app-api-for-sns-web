#KOL APP API - get_message_list
##接口描述：获取某个app kol 消息列表

```
Request Parameters -
snsUser :  sns web 登录帐号
senderUUID : A unique id to identify who is sending this message or event id，Should be md5(snsUser+sendAs)
appKolId ； app kol id
onlyUnRead: 是否只返回未读消息,如果为ture，要把所有的未读信息一次返回，不用管pageNum和pageSize，如果为false，按照pageNUm和pageSize返回信息列表
pageNum : 页码
pageSize : 每页记录数

举例
{
"snsUser":"BJRND3@admin",
"senderUUID":"68565fbee77756886176d373ff679c7d",
"appKolId":11,
"onlyUnRead"：true,
"pageNum":1，
"pageSize":20
}

```

----------------------------------------------------------------------

```
Reponse Parameters -
returnCode : Success:50000,Fail:40000,Error:以"E"开头的编码
errorMsg :  如果returnCode返回为非Success,则需要填写相应的错误提示信息
senderUUID : A unique id to identify who is sending this message or event id，Should be md5(snsUser+sendAs)
messages : 消息列表
id : message or event id，由app 查询event接口提供，供event追加发送kol时使用
msgType : 消息内容类型，有两种类型message或event
sendAs : 发消息人昵称
title : 标题，type=event时有值，type=message时为空
snsUser :  sns web 登录帐号
createAt : 消息创建时间
attachment : 附件，一个消息可以发送一张图片(发送新消息) 或文件名称(event追加发送kol),也可以为null
content : 消息正文
read：是否已读，0，未读，1已读

举例
{
  "returnCode":"50000",
  "errorMsg":null,
  "senderUUID":"68565fbee77756886176d373ff679c7d",
  "messages":[
    {
      "id":1,
      "msgType":"event",
      "sendAs":"wisers kol",
      "title":"推广",
      "snsUser":"BJRND3@admin",
      "createAt":"2016-11-10 14:55:22",
      "attachment":"http://www.image.com/image.png",
      "content":"event",
      "read":0
    },
    {
      "id":2,
      "msgType":"message",
      "sendAs":"wisers kol",
      "title":null,
      "snsUser":"BJRND3@admin",
      "createAt":"2016-11-10 14:55:22",
      "attachment":"http://www.image.com/image.png",
      "content":"event",
      "read":1
    }
  ]
}
```