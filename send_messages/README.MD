#KOL APP API - send_messages
##接口描述：用于sns web 客户给app kol发送品牌推广信息


```
Request Parameters -
id : message or event id，由app 查询event接口提供，供event追加发送kol时使用
kolAppIds : kol app id列表，可以多个id表示广播，可以传单个id表示私聊
senderUUID : A unique id to identify who is sending this message or event id，Should be md5(snsUser+sendAs)
msgType : 消息内容类型，有两种类型message或event
sendAs : 发消息人昵称
title : 标题，type=event时有值，type=message时为空
snsUser :  sns web 登录帐号
createAt : 消息创建时间
attachment : 附件，一个消息可以发送一张图片(发送新消息) 或文件名称(event追加发送kol),也可以为null
content : 消息正文
action : 消息发送类型，是新发送(new)or追加(addtition)

举例
{
    "id":null,
   "kolAppIds":"1,2,3",
   "senderUUID":"68565fbee77756886176d373ff679c7d",
   "msgType":"event",
   "sendAs":"wisers kol",
   "title":"推广",
   "snsUser":"BJRND3@admin"
   "createAt":"2016-11-10 14:55:22",
   "attachment":"base64(image) or file name",
   "content":"event"
   "action":"new"
}
```

--------------------------------------------------

```
Reponse Parameters -
returnCode : Success:50000,Fail:40000,Error:以"E"开头的编码
errorMsg :  如果returnCode返回为非Success,则需要填写相应的错误提示信息
senderUUID : A unique id to identify who is sending this message or event id，Should be md5(snsUser+sendAs)
successIds : 消息推送成功kol app ids
failedIds :  消息推送失败kol app ids

举例
{
 "returnCode":"50000",
 "errorMsg":null,
 "senderUUID":"68565fbee77756886176d373ff679c7d",
 "successIds":"1,2,3",
 "failedIds":null
}
```

------------------------------------------------------