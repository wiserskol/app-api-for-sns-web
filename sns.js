var express = require('express');
var bodyParser = require('body-parser');
var path = require('path');
var _ = require('underscore');
var app = express();
var md5 = require('blueimp-md5')

const secret='35a2b0a2d88ca8ca89f5da0d8fce46c7'

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));

function validateSecret(req) {
    const securityCode = req.get('X-SECURITY-CODE')
    const time = req.get('X-SECURITY-TIME')
    const hash = md5(`${secret}|${time}`)
    return (hash == securityCode)
}

app.post('/get_brands', function(req, res) {
         console.log("call get_brands", req.body);
         
         if (!validateSecret(req)) {
            res.json({"returnCode":"E10001","action":"get_brands"});
            return;
         }
         
         if (_.isUndefined(req.body.wisers_kol_id)) {
            res.json({"returnCode":"E10004","action":"get_brands"});
            return;
         }
         
         res.sendFile(path.join(__dirname+'/get_brands/sample.json'));
});

app.post('/get_interests_industries', function(req, res) {
         console.log("call get_interests_industries", req.body);

         if (!validateSecret(req)) {
            res.json({"returnCode":"E10001","action":"get_interests_industries"});
            return;
         }

         if (_.isUndefined(req.body.wisers_kol_id)) {
            res.json({"returnCode":"E10004","action":"get_interests_industries"});
            return;
         }
         
         res.sendFile(path.join(__dirname+'/get_interests_industries/sample.json'));
});

app.post('/get_post_list', function(req, res) {
         console.log("call get_post_list", req.body);
         
         if (!validateSecret(req)) {
            res.json({"returnCode":"E10001","action":"get_post_list"});
            return;
         }

         if (_.isUndefined(req.body.pageNum) || _.isUndefined(req.body.pageSize) || _.isUndefined(req.body.wisers_kol_id) || _.isUndefined(req.body.industries) || _.isUndefined(req.body.keyword) || _.isUndefined(req.body.type)) {
            res.json({"returnCode":"E10004","action":"get_post_list"});
            return;
         }
         
         if (req.body.type == "feed" || req.body.type == "kol" || req.body.type == "brand") {
            res.json({"returnCode":"E40000","action":"get_post_list"});
            return;
         }
         
         res.sendFile(path.join(__dirname+'/get_post_list/sample.json'));
});

app.post('/get_post_with_ids', function(req, res) {
         console.log("call get_post_with_ids", req.body);
         
         if (!validateSecret(req)) {
            res.json({"returnCode":"E10001","action":"get_post_with_ids"});
            return;
         }

         if (_.isUndefined(req.body.wisers_kol_id) || _.isUndefined(req.body.postId)) {
            res.json({"returnCode":"E10004","action":"get_post_with_ids"});
            return;
         }
         
         if (req.body.postId.length < 1) {
            res.json({"returnCode":"E40000","action":"get_post_with_ids"});
            return;
         }
         
         res.sendFile(path.join(__dirname+'/get_post_with_ids/sample.json'));
});

app.listen(8080, function() {
    console.log('KOL SNS API listening on port 8080')
});
